import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;


public class VideoReportGenerator2 {

	static Date startDate = null;
	static Date endDate = null;
	static JsonArray accounts = new JsonArray();
	static String startDateStr = "09-02-2020 00:00:00";
	static String endDateStr = "15-02-2020 23:59:59";
	static SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
	
	static String jsScript = "for(videos=document.querySelectorAll(\"c-wiz [data-show-delete-individual][data-date]\"),data=\"\",data_json={},j=0,search=\"\",account=document.querySelectorAll('[aria-label=\"Account Information\"]')[0].children[0].children[1].children[0].innerText,i=0;i<videos.length;i++){if(-1==videos[i].innerText.indexOf(\"Searched\")&&0!=videos[i].querySelectorAll('[aria-label=\"Video duration\"]').length)s=(s=(s=(s=(s=(s=(s=videos[i].innerText).replace(/Watched /g,\"\")).replace(/ • Details/g,\"\")).replace(/YouTube\\n/g,\"\")).replace(/\\n/g,\"\")).replace(/\\n/g,\"\")).replace(/\\n/g,\"\"),null!=videos[i].querySelectorAll('[aria-label=\"Video duration\"]')[0].nextElementSibling?(watchedPercentage=videos[i].querySelectorAll('[aria-label=\"Video duration\"]')[0].nextElementSibling.nextElementSibling.style.width,data+=s+\"\\n\"+videos[i].getElementsByTagName(\"a\")[0].getAttribute(\"href\").split(\"&\")[0]+\"\\n\"+videos[i].getAttribute(\"data-date\")+\"\\n\"+watchedPercentage+\"\\n\\n\"):data+=s+\"\\n\"+videos[i].getElementsByTagName(\"a\")[0].getAttribute(\"href\").split(\"&\")[0]+\"\\n\"+videos[i].getAttribute(\"data-date\")+\"\\n100%\\n\\n\",j++;else if(-1!=videos[i].innerText.indexOf(\"Searched\")){var s;s=(s=(s=(s=(s=(s=videos[i].innerText).replace(/YouTube\\n/g,\"\")).replace(/\\n/g,\"\")).replace(/\\n/g,\"\")).replace(/ • Details/g,\"\")).replace(/Searched for /g,\"\"),search+=s+\"\\n\"+videos[i].getAttribute(\"data-date\")+\"\\n\\n\"}else console.log(\"This content is neither a search nor a watched video, it could be video without duration or live video\")}console.log(data),console.log(search);";
	static int startIndex = 0;
	
	static WebDriver driver = null;
	static JavascriptExecutor js = null;
	static String failedVideoReport="";
	static String noVideoReport="";
	static String succeededVideoReport="";
	
	static String failedSearchReport="";
	static String noSearchReport="";
	static String succeededSearchReport="";
	
	static double totalVideos=0;
	static double totalSearches=0;
	
	static HashMap<String, Integer> vidCountsByAcc = new HashMap<String, Integer>();
	static HashMap<String, Integer> srchCountsByAcc = new HashMap<String, Integer>();
	
	static JsonObject reportJson = new JsonObject();
	//static JsonObject stateJson = new JsonObject();
	static JsonObject disttJson = new JsonObject();
	//static JsonObject tehsilJson = new JsonObject();
	
	static String cookiesExpiryDate = "Mon, 20 Dec 2022 16:45:14 GMT";
	
	public static void main(String[] args) throws IOException {
		String startTime = new Date().toGMTString();
		initializeAccounts();System.out.println(accounts);
		try {
			startDate = sdf.parse(startDateStr);
			endDate = sdf.parse(endDateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		System.out.println(startDate + "----------" + endDate);
		System.out.println(startDate.getTime() + "000" + "-----------" + endDate.getTime() + "000");
		
		VideoReportGenerator2 vrg = new VideoReportGenerator2();
		startIndex=accounts.size()-2;
		for(int i=startIndex;i<accounts.size();i++) {
			JsonObject account = accounts.get(i).getAsJsonObject();
			String user=account.get("id").getAsString();
			String pwd=account.get("pwd").getAsString();
			System.out.println(user);
			/*if(!user.contains("harrakhedi")) {
				System.out.println(user);
				continue;
			}*/
			
			System.out.println("Videos Status "+i+"/"+accounts.size()+"- \nSucceeded: "+"("+totalVideos+") - "+succeededVideoReport+"\nNoReport: "+noVideoReport+"\nFailed: "+failedVideoReport);
			System.out.println("Searches Status "+i+"/"+accounts.size()+"- \nSucceeded: "+"("+totalSearches+") - "+succeededSearchReport+"\nNoReport: "+noSearchReport+"\nFailed: "+failedSearchReport);
			if(i!=(accounts.size()-1))
				waitFor(5);
			System.out.println("Account: "+user+" - Initializing driver........");
			initializeDriver();
			if(account.get("loginType").getAsString().equalsIgnoreCase("cookie")) {
				vrg.fetchData(account);
			}else if(account.get("loginType").getAsString().equalsIgnoreCase("credentials")) {
				if(vrg.login(user, pwd)) {
					waitFor(5);
					vrg.fetchData(account);
				}
			}
			
			driver.quit();
			System.out.println("Account: "+user+"Fetching data finished. Quiting driver........");
		}
		System.out.println("Videos Status - \nSucceeded: "+"("+totalVideos+") - "+succeededVideoReport+"\nNoReport: "+noVideoReport+"\nFailed: "+failedVideoReport);
		System.out.println("Searches Status - \nSucceeded: "+"("+totalSearches+") - "+succeededSearchReport+"\nNoReport: "+noSearchReport+"\nFailed: "+failedSearchReport);
		sortByValue(vidCountsByAcc);
		sortByValue(srchCountsByAcc);
		System.out.println("VideoCountsByAcc: "+vidCountsByAcc);
		System.out.println("srchCountsByAcc: "+srchCountsByAcc);
		System.out.println("Start Time: "+startTime+", Ending Time: "+new Date().toGMTString());
		
		generateReport(vidCountsByAcc, accounts);
	}

	public void fetchData(JsonObject acc) throws IOException {
		String user = acc.get("id").getAsString();
		String accName = acc.get("name").getAsString();
		String authuser = acc.get("authuser").getAsString();
		String district = acc.get("district").getAsString();
		
		driver.get("https://www.google.com/");
		System.out.println("google browsed.............");
		driver.manage().addCookie(new Cookie("HSID", acc.get("hsid").getAsString(), ".google.com", "/", new Date(cookiesExpiryDate)));
		driver.manage().addCookie(new Cookie("SID", acc.get("sid").getAsString(), ".google.com", "/", new Date(cookiesExpiryDate)));
		driver.manage().addCookie(new Cookie("SSID", acc.get("ssid").getAsString(), ".google.com", "/", new Date(cookiesExpiryDate)));
		waitFor(5);
		
		/*
		 * String pageTitle = (String)js.executeScript("return document.title");
		 * if(pageTitle!="Google - My Activity") { String url = (String)
		 * js.executeScript("document.URL"); System.out.println("Wrong page: "+url);
		 * failedReport+=user+", "; return; }
		 */
		
		driver.get("https://myactivity.google.com/activitycontrols/youtube?authuser="+authuser+"&restrict=youtube&min=" + startDate.getTime()
		+ "000" + "&max=" + endDate.getTime() + "000");
		waitFor(10);
		
		String openedAcc = (String)js.executeScript("return document.querySelectorAll('[aria-label=\"Account Information\"]')[0].children[0].children[1].children[1].innerText");
		if(!openedAcc.equalsIgnoreCase(user+"@gmail.com")) {
			System.out.println("There is an issue with this login: opened acc:"+openedAcc + ", Expected Account: "+user);
			failedVideoReport +=user+", ";
			return;
		}
		
		Long last_height = (Long) js.executeScript("return document.body.scrollHeight");
		System.out.println(last_height);
				
		while(true) {
			js.executeScript("window.scrollTo(0, document.body.scrollHeight);");
			waitFor(5);
			Long new_height = (Long) js.executeScript("return document.body.scrollHeight");
			if(new_height.equals(last_height)) {
				break;
			}
			last_height = new_height;
			System.out.println("last_height: "+last_height+" - Scrolling for more results.......... Setting new height: "+new_height);
		}
		
		String vData = null;String sData = null;String vReport="";String sReport="";
		try {
			
			sData = (String) js.executeScript(jsScript+"return search;");
			vData = (String) js.executeScript(jsScript+"return data;");
			//System.out.println("vData: "+vData);System.out.println("sData: "+sData);// account info get & to be double checked 
			
			if(vData == null) {
				System.out.println("#######################################################vData: "+vData);
				failedVideoReport +=user+", ";
			}else if(vData.isEmpty()) {
				noVideoReport +=user+", ";
			}else {
				String rows[] = vData.split("\n\n");
				//System.out.println("======================================"+Arrays.asList(rows)+"===================================");
				int above49P = 0;
				for(int i=0; i<rows.length;i++) {
					String[] cells = rows[i].split("\n");
					
					if(cells.length==7) {
						String channel = cells[1].replace("~", "_"); String title = cells[0].replace("~", "_"); String time = cells[2]; String length = cells[3];String url = cells[4]; String date=cells[5]; String vPercent=cells[6];
						
						String subject = "";//getSubject(title);
						int p = Integer.parseInt(vPercent.replace("%", ""));
						if(p>49) {
							above49P++;
						}
						if(!subject.equals("") && subject!=null)
							vReport+=accName+"~"+district+"~"+channel+"~"+title+"~"+date+"~"+time+"~"+length+"~"+vPercent+"~"+subject+"~"+url+"\n";
						else
							vReport+=accName+"~"+district+"~"+channel+"~"+title+"~"+date+"~"+time+"~"+length+"~"+vPercent+"~"+url+"\n";
					} else {
						System.out.println(i+": Something wrong here.... "+Arrays.asList(cells));
					}
				}
				writeToFile(vReport, "C:\\Users\\ajayyadav\\Downloads\\a.csv");
				succeededVideoReport +=user+"("+rows.length+"), ";totalVideos += rows.length;
				//reportJson.addProperty(accName, rows.length);
				vidCountsByAcc.put(user, above49P);
				//addToReport(acc, rows.length);
			}
			
			if(sData == null) {
				System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$sData: "+sData);
				failedSearchReport +=user+", ";
			}else if(sData.isEmpty()) {
				noSearchReport +=user+", ";
			}else {
				String rows[] = sData.split("\n\n");
				for(int i=0; i<rows.length;i++) {
					String[] cells = rows[i].split("\n");
					
					if(cells.length==3) {
						String sKeywords = cells[0]; String time = cells[1]; String date=cells[2];
						sReport+=accName+"~"+district+"~"+sKeywords+"~"+date+"~"+time+"\n";
					}else {
						System.out.println(i+": Something wrong here.... "+Arrays.asList(cells));
					}
				}
				writeToFile(sReport, "C:\\Users\\ajayyadav\\Downloads\\b.csv");
				succeededSearchReport +=user+"("+rows.length+"), ";totalSearches += rows.length;
				srchCountsByAcc.put(user, rows.length);
			}
			
		}catch(org.openqa.selenium.JavascriptException e) {
			System.out.println(e.getMessage()+"=====Javascript execution failed. Skipping: "+user);
			System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!vData: "+vData);System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!sData: "+sData);
			failedSearchReport +=user+", ";
		}/*catch(Exception e) {
			System.out.println(e.getMessage()+"=====Error due to unhandled exception("+e.getClass().getCanonicalName()+"). Skipping: "+user);
			System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!vData: "+vData);System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!sData: "+sData);
			failedSearchReport +=user+", ";
		}*/
		
	}
	
	private String getSubject(String title) {
		String sub = "";
		title = title.toLowerCase();
		if(title.contains("Story")|| title.contains("Stories") || title.contains("Story")) {
			return "Story";
		}//else if
		Pattern p = Pattern.compile("[a-zA-Z]+"); 
		Matcher m = p.matcher(title); 
		while (m.find()) { 
			System.out.println(m.group()); 
		} 
		return null;
	}

	private void addToReport(JsonObject acc, int length) {
		String accid = acc.get("id").getAsString();
		String accName = acc.get("name").getAsString();
		String district = acc.get("district").getAsString();
		vidCountsByAcc.put(accName, length);
		
		reportJson.addProperty("videos", reportJson.get("videos").getAsInt()+length);
		
		JsonObject accJson = reportJson.get("byAccounts").getAsJsonObject();
		accJson.addProperty(accid, length);
		reportJson.add("byAccounts", accJson);
		
		if(disttJson.get(district).isJsonNull()) {
			JsonObject dJson = new JsonObject();
			dJson.addProperty("_count", 0);
			disttJson.add(district, dJson);
		}
		JsonObject dJson = disttJson.get(district).getAsJsonObject();
		dJson.addProperty("_count", dJson.get("_count").getAsInt()+length);
		dJson.addProperty(accid, length);
		
		disttJson.add(district, dJson);
	}
	
	private static void generateReport(HashMap<String, Integer> vidCountsByAcc, JsonArray accounts) {
		JsonObject byDistrict = new JsonObject();
		for(int i=startIndex;i<accounts.size();i++) {
			String distt = accounts.get(i).getAsJsonObject().get("district").getAsString();
			String id = accounts.get(i).getAsJsonObject().get("id").getAsString();
			//System.out.println("distt: "+distt+", id: "+id);
			if(!byDistrict.has(distt)) {
				JsonObject dJson = new JsonObject();
				dJson.addProperty("_count", 0);
				byDistrict.add(distt, dJson);
			}
			//System.out.println("byDistrict: "+byDistrict);
			JsonObject dJson = byDistrict.get(distt).getAsJsonObject();
			if(vidCountsByAcc.get(id) != null) {
				dJson.addProperty("_count", dJson.get("_count").getAsInt()+vidCountsByAcc.get(id));
				dJson.addProperty(id, vidCountsByAcc.get(id));
			}else {
				System.out.println("No videos seen by account: "+id);
			}
		}
		System.out.println("Report: "+byDistrict.toString());
	}
	
	public static void initializeJson() {
		reportJson.addProperty("videos", 0);
		
		//reportJson.add("byState", stateJson);
		//reportJson.add("byState", disttJson);
		//reportJson.add("byState", tehsilJson);
	}

	public boolean login(String user, String pwd) throws IOException {
		
		String url = "https://accounts.google.com/signin";
		driver.get(url);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		WebElement email_phone = driver.findElement(By.xpath("//input[@id='identifierId']"));
		email_phone.sendKeys(user);
		driver.findElement(By.id("identifierNext")).click();
		WebElement password = driver.findElement(By.xpath("//input[@name='password']"));
		waitFor(5);
		password.sendKeys(pwd);
		driver.findElement(By.id("passwordNext")).click();
		System.out.println(driver.getTitle());
		
		waitFor(5);
		String title = (String) js.executeScript("return document.title");
		if(title.contains("Sign in")) {
			System.out.println("LOGIN FAILED........!!!!!");
			failedVideoReport+=user+", ";
			return false;
		}else
			return true;
		
	}

	public static void writeToFile(String textToAppend, String file) throws IOException {
		Path path = Paths.get(file);
		Files.write(path, textToAppend.getBytes(), StandardOpenOption.APPEND); // Append mode
	}
	
	public static void waitFor(int s) {
		try {
			Thread.sleep(s * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void initializeDriver() {
		System.setProperty("webdriver.gecko.driver", "C:\\Users\\ajayyadav\\Downloads\\geckodriver.exe");
		driver = new FirefoxDriver();
		js = (JavascriptExecutor) driver;
	}
	
	public static HashMap<String, Integer> sortByValue(HashMap<String, Integer> hm) { 
        // Create a list from elements of HashMap 
        List<Map.Entry<String, Integer> > list = 
               new LinkedList<Map.Entry<String, Integer> >(hm.entrySet()); 
  
        // Sort the list 
        Collections.sort(list, new Comparator<Map.Entry<String, Integer> >() { 
            public int compare(Map.Entry<String, Integer> o1,  
                               Map.Entry<String, Integer> o2) 
            { 
                return (o1.getValue()).compareTo(o2.getValue()); 
            } 
        }); 
          
        // put data from sorted list to hashmap  
        HashMap<String, Integer> temp = new LinkedHashMap<String, Integer>(); 
        for (Map.Entry<String, Integer> aa : list) { 
            temp.put(aa.getKey(), aa.getValue()); 
        } 
        return temp; 
    } 
	
	private static void initializeAccounts() {
		//111111111111111111111
		String hsid = "AGBvqhsU4FAHHJfZ0";String sid="rgcN5VfaTn6U60QPGBCAJivGYUlSP-lF_WDMlkI0QWpFQ2LaLnwZ2ihqAiJiO_KjDKbCJg.";String ssid="AUL-k5tye7qv9XAGW";
		JsonObject sa=new JsonObject();
		sa.addProperty("id", "gmssilkho.smartclass");
		sa.addProperty("name", "GMS Silkho, Nuh");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "1");
		sa.addProperty("district", "Nuh");
		accounts.add(sa);
		
		sa=new JsonObject();
		sa.addProperty("id", "gmschilla.smartclass");
		sa.addProperty("name", "GMS Chilla");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "0");
		sa.addProperty("district", "Nuh");
		accounts.add(sa);
		
		sa=new JsonObject();
		sa.addProperty("id", "gmsgurnawat.smartclass");
		sa.addProperty("name", "GMS Gurnawat");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "2");
		sa.addProperty("district", "Nuh");
		accounts.add(sa);
		
		sa=new JsonObject();
		sa.addProperty("id", "gmschilawali.smartclass");
		sa.addProperty("name", "GMS Chilawali");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "3");
		sa.addProperty("district", "Nuh");
		accounts.add(sa);
		
		sa=new JsonObject();
		sa.addProperty("id", "gsss.kadarpur.gurugram.dsc");
		sa.addProperty("name", "GSSS Kadarpur Gurugram");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "4");
		sa.addProperty("district", "Gurugram");
		accounts.add(sa);
		
		sa=new JsonObject();
		sa.addProperty("id", "gps.tikri.gurugram.dsc");
		sa.addProperty("name", "GPS Tikri Gurugram");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "6");
		sa.addProperty("district", "Gurugram");
		accounts.add(sa);
		
		sa=new JsonObject();
		sa.addProperty("id", "ggms.sakras.nuh.dsc");
		sa.addProperty("name", "GGMS Sakras Nuh");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple123!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "8");
		sa.addProperty("district", "Nuh");
		accounts.add(sa);
		
		sa=new JsonObject();
		sa.addProperty("id", "gps.lidhora.jhansi.dsc");
		sa.addProperty("name", "GPS Lidhora Jhansi");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "7");
		sa.addProperty("district", "Jhansi");
		accounts.add(sa);
		
		//2222222222222222222222222
		hsid = "AYehAx-3V0Scipqt6";sid="sQcfJ1MO-BoRZhZs-HP1Wxj9DMnWfnYRyoP7W8Z9NmzXZic4_nIH8k53jSoCrRIpEUTJlA.";ssid="AeaM4Fs1C4qREr3rc";
		sa=new JsonObject();
		sa.addProperty("id", "ghs.kanhai.gurugram.dsc");
		sa.addProperty("name", "GHS Kanhai Gurugram");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "0");
		sa.addProperty("district", "Gurugram");
		accounts.add(sa);
		
		sa=new JsonObject();
		sa.addProperty("id", "ghss.amraibag.bhopal.dsc");
		sa.addProperty("name", "GHSS Amrai Bag Sewania, Bhopal");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "1");
		sa.addProperty("district", "Bhopal");
		accounts.add(sa);
		
		sa=new JsonObject();
		sa.addProperty("id", "gmlb.ghss.berkheda.bhopal.dsc");
		sa.addProperty("name", "Govt MLB Girls Higher Secondary School, Berkheda, Bhopal");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "2");
		sa.addProperty("district", "Bhopal");
		accounts.add(sa);
		
		sa=new JsonObject();
		sa.addProperty("id", "ghss.bawadia.bhopal.dsc");
		sa.addProperty("name", "GHSS Bawadia, Bhopal");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "3");
		sa.addProperty("district", "Bhopal");
		accounts.add(sa);
		//==========================333333333333333333
		hsid = "ALnGQ4Fwqxuif0x95";sid="tAdCD4cfm6CObdJ-p0QKa7zLKk-FcDP9FZ-46ADIcXvHCSSaUL5enMwAP6ZZEofVBCDfZQ.";ssid="AhMuIUBPDd2tI9-qo";
		
		sa=new JsonObject();
		sa.addProperty("id", "gms.khuwajlikalan.punhana.nuh.dsc");
		sa.addProperty("name", "GGPS Jamalgarh, Punhana, Nuh");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "0");
		sa.addProperty("district", "Nuh");
		accounts.add(sa);
		
		sa=new JsonObject();
		sa.addProperty("id", "ggsss.shikrawa.punhana.nuh.dsc");
		sa.addProperty("name", "GGSSS Shikrawa Punhana");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "1");
		sa.addProperty("district", "Nuh");
		accounts.add(sa);
		
		sa=new JsonObject();
		sa.addProperty("id", "gps.shikrawa.punhana.nuh.dsc");
		sa.addProperty("name", "GPS Shikrawa Punhana");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "2");
		sa.addProperty("district", "Nuh");
		accounts.add(sa);
		
		sa=new JsonObject();
		sa.addProperty("id", "gms.badholi.punhana.nuh.dsc");
		sa.addProperty("name", "GMS Badholi Punhana");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "3");
		sa.addProperty("district", "Nuh");
		accounts.add(sa);
		
		sa=new JsonObject();
		sa.addProperty("id", "gms.badha.punhana.nuh.dsc");
		sa.addProperty("name", "GMS Badha Punhana");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "4");
		sa.addProperty("district", "Nuh");
		accounts.add(sa);
		
		sa=new JsonObject();
		sa.addProperty("id", "gps.nawalgarh.punhana.nuh.dsc");
		sa.addProperty("name", "GPS Nawalgarh Punhana");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "5");
		sa.addProperty("district", "Nuh");
		accounts.add(sa);
		
		sa=new JsonObject();
		sa.addProperty("id", "ggsss.bisru.punhana.nuh.dsc");
		sa.addProperty("name", "GGMS Jamalgarh, Punhana, Nuh");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "6");
		sa.addProperty("district", "Nuh");
		accounts.add(sa);
		
		sa=new JsonObject();
		sa.addProperty("id", "ghasss.shahchokha.punhana.nuh.dsc");
		sa.addProperty("name", "GHASSS Shahchokha Punhana");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "7");
		sa.addProperty("district", "Nuh");
		accounts.add(sa);
		
		//=====================================******************4444444444
		
		hsid = "AiTuUlR6LNBX-7NAu";sid="tQcRYKP6IulLmyqtplD5iXV3W_BhOJtLy7yJi5mzRGSUgJTphpr5FUqoTS-gt0uOYpxJuQ.";ssid="ARIMz9aiX8F2a3MJ1";
		
		sa=new JsonObject();
		sa.addProperty("id", "gms.sewka.nuh.dsc");
		sa.addProperty("name", "GMS Sewka Nuh");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "0");
		sa.addProperty("district", "Nuh");
		accounts.add(sa);
		
		sa=new JsonObject();
		sa.addProperty("id", "ghss.rehda.hbd.mp.dsc");
		sa.addProperty("name", "GHSS Rehda HBD MP");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "1");
		sa.addProperty("district", "Bhopal");
		accounts.add(sa);
		
		sa=new JsonObject();
		sa.addProperty("id", "gghss.seoni.hoshangabad.bhopal.dsc");
		sa.addProperty("name", "GGHSS Seoni Bhopal");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "2");
		sa.addProperty("district", "Bhopal");
		accounts.add(sa);
		
		sa=new JsonObject();
		sa.addProperty("id", "ghss.shivpur.hshngbd.bhopal.dsc");
		sa.addProperty("name", "GHSS Shivpur Bhopal");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "3");
		sa.addProperty("district", "Bhopal");
		accounts.add(sa);
		
		sa=new JsonObject();
		sa.addProperty("id", "ghss.khapariya.hshngbd.bhopal.dsc");
		sa.addProperty("name", "GHSS Khapariya Bhopal");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "4");
		sa.addProperty("district", "Bhopal");
		accounts.add(sa);
		
		sa=new JsonObject();
		sa.addProperty("id", "ghss.malapat.hshngbd.bhopal.dsc");
		sa.addProperty("name", "GHSS Malapat Bhopal");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "5");
		sa.addProperty("district", "Bhopal");
		accounts.add(sa);
		
		sa=new JsonObject();
		sa.addProperty("id", "gnhs.babri.hshngbd.bhopal.dsc");
		sa.addProperty("name", "GNHS Babri Bhopal");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "6");
		sa.addProperty("district", "Bhopal");
		accounts.add(sa);
		
		sa=new JsonObject();
		sa.addProperty("id", "ghss.baghwada.hshngbd.bhopal.dsc");
		sa.addProperty("name", "GHSS Baghwada Bhopal");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "7");
		sa.addProperty("district", "Bhopal");
		accounts.add(sa);
		
		sa=new JsonObject();
		sa.addProperty("id", "ghss.chtrkheds.hshngbd.bhopal.dsc");
		sa.addProperty("name", "GHSS Chatarkheds Bhopal");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "8");
		sa.addProperty("district", "Nuh");
		accounts.add(sa);
		
		sa=new JsonObject();
		sa.addProperty("id", "ghss.somalwada.hshngbd.bhopal.dsc");
		sa.addProperty("name", "GHSS Somalwada Bhopal");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "9");
		sa.addProperty("district", "Bhopal");
		accounts.add(sa);
		
		//=========================
		
		//=====================================******************555555
	
		hsid = "A6557dhTcrUr1-kZ4";sid="tQd2QtprdkCdgg3wqNT3KZlvvcfYop6QNhK0ukEyHEaUsGZ6ZMy3cRxlX5gaUJVykxAx9A.";ssid="AgBH9s7wD-Jf5yuNv";
		
		sa=new JsonObject();
		sa.addProperty("id", "gehss.seoni.hshngbd.bhopal.dsc");
		sa.addProperty("name", "G Excellance HSS Seoni Bhopal");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "0");
		sa.addProperty("district", "Bhopal");
		accounts.add(sa);
		
		sa=new JsonObject();
		sa.addProperty("id", "gmlbhss.hatta.damoh.mp.dsc");
		sa.addProperty("name", "GMLBHSS Hatta Damoh MP");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "1");
		sa.addProperty("district", "Damoh");
		accounts.add(sa);
		
		sa=new JsonObject();
		sa.addProperty("id", "ghss.chunabhati.bhopal.mp.dsc");
		sa.addProperty("name", "GHSS Chunabhati Bhopal");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "2");
		sa.addProperty("district", "Bhopal");
		accounts.add(sa);
		
		sa=new JsonObject();
		sa.addProperty("id", "ghs.dhamarra.bhopal.mp.dsc");
		sa.addProperty("name", "GHS Dhamarra Bhopal");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "3");
		sa.addProperty("district", "Bhopal");
		accounts.add(sa);
		
		sa=new JsonObject();
		sa.addProperty("id", "ms.damoh.mp.dsc");
		sa.addProperty("name", "MS Damoh MP");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "4");
		sa.addProperty("district", "Damoh");
		accounts.add(sa);
		
		sa=new JsonObject();
		sa.addProperty("id", "gghssmlb.damoh.mp.dsc");
		sa.addProperty("name", "GGHSSMLB Damoh MP");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "5");
		sa.addProperty("district", "Damoh");
		accounts.add(sa);
		
		sa=new JsonObject();
		sa.addProperty("id", "jhs.damoh.mp.dsc");
		sa.addProperty("name", "JHS Damoh");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "6");
		sa.addProperty("district", "Damoh");
		accounts.add(sa);
		
		sa=new JsonObject();
		sa.addProperty("id", "gms.ganiyari.jbp.dsc");
		sa.addProperty("name", "GMS Ganiyari Jabalpur");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "7");
		sa.addProperty("district", "Jabalpur");
		accounts.add(sa);
		
		sa=new JsonObject();
		sa.addProperty("id", "gps.rrcamp.ggn.dsc");
		sa.addProperty("name", "GPS RR Camp Gurugram ");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "9");
		sa.addProperty("district", "Gurugram");
		accounts.add(sa);
		
		//66666666666666666666666666666666666666666666
		hsid = "A-3TtBzx0oskl_uj_";sid="tgddCrAwsjzWVVvQXWFvlI0v7baMi8xfGuGPJZ34X_6BK9NZ7p55Id925e2rJJD3pyRa6g.";ssid="A-gVqBNKQmtqkeAd3";
		
		sa=new JsonObject();
		sa.addProperty("id", "ghss.harrakhedi.bpl.dsc");
		sa.addProperty("name", "GHSS Harrakhedi Bhopal");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "0");
		sa.addProperty("district", "Bhopal");
		accounts.add(sa);
		
		sa=new JsonObject();
		sa.addProperty("id", "gms.carterpuri.ggn.dsc");
		sa.addProperty("name", "GMS Carterpuri Gurugram");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "2");
		sa.addProperty("district", "Gurugram");
		accounts.add(sa);
		

		sa=new JsonObject();
		sa.addProperty("id", "gms.neemka.punhana.nuh.dsc");
		sa.addProperty("name", "GMS Neemka Punhana");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "3");
		sa.addProperty("district", "Nuh");
		accounts.add(sa);
		
		sa=new JsonObject();
		sa.addProperty("id", "gps.bhajlaka.nuh.dsc");
		sa.addProperty("name", "GPS Bhajlaka Nuh");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "4");
		sa.addProperty("district", "Nuh");
		accounts.add(sa);
		
		hsid = "AoaRKW3MJ1Jh7d64J";sid="twd8ssa13FUTGwvpNT1Qbwv3l5JDmx7Q74R7RCmShR12ENyAh8CRJv3Hu6U2JOI2qX0s2g.";ssid="AU-U2Jx2pzOeTBVf-";
		
		sa=new JsonObject();
		sa.addProperty("id", "gps.shushantlok.ggn.dsc");
		sa.addProperty("name", "GPS Sushantlok, Gurgaon, Haryana");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "0");
		sa.addProperty("district", "Gurugram");
		accounts.add(sa);
		
		sa=new JsonObject();
		sa.addProperty("id", "gps.mpmeo.ggn.dsc");
		sa.addProperty("name", "GPS MP Meo, Sohna, Gurgaon, Haryana");
		sa.addProperty("loginType", "cookie");
		sa.addProperty("pwd", "simple1!");
		sa.addProperty("hsid", hsid);
		sa.addProperty("sid", sid);
		sa.addProperty("ssid", ssid);
		sa.addProperty("authuser", "1");
		sa.addProperty("district", "Gurugram");
		accounts.add(sa);
	}
	
	

}
